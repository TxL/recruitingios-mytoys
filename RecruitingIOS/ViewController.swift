//
//  ViewController.swift
//  RecruitingIOS
//
//  Created by TxL PRO on 16/9/16.
//  Copyright © 2016 mytoys. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet var myWebView: UIWebView!
    
    var dict : NSDictionary!
    var config : NSDictionary!

    // MARK: Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
  
        // Additional bar button items, set just initial menu button
        let menu = UIBarButtonItem(image: UIImage(named: "menu"), style: .Plain, target: self, action: #selector(ViewController.goMenu))
        navigationItem.leftItemsSupplementBackButton = true
        navigationItem.setLeftBarButtonItem(menu, animated: true)
     
        // Initialize user defaults
        NSUserDefaults.standardUserDefaults().setInteger(0, forKey: "backMain")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        // Register event to refresh webview url
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(refreshWeb), name: "RefreshWeb", object: nil)
        
        // read config dict
        let path = NSBundle.mainBundle().pathForResource("Config", ofType: "plist")
        self.config = NSDictionary(contentsOfFile: path!)
        
        // init webview with initial url
        let url = NSURL (string: self.config.valueForKey("main_url") as! String)
        let requestObj = NSURLRequest(URL: url!)
        myWebView.loadRequest(requestObj)
        myWebView.delegate = self
        
        // app style, logo on header approach
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        imageView.contentMode = .ScaleAspectFit
        let image = UIImage(named: "logo")
        imageView.image = image
        self.navigationItem.titleView = imageView
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        makeRequest()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // method to make API REST request
    func makeRequest() {
        
        // make API REST request
        let urlPath: String = self.config.valueForKey("api_url") as! String
        let url: NSURL = NSURL(string: urlPath)!
        let request1: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        
        // set API KEY in header
        request1.setValue(self.config.valueForKey("api_key") as? String, forHTTPHeaderField: "x-api-key")
        
        // make async request, parse JSON and store result
        let task = session.dataTaskWithRequest(request1, completionHandler:{(data, response, error) in
            if error != nil{
                print(error!.localizedDescription)
                return
            }
            if let responseJSON = try! NSJSONSerialization.JSONObjectWithData(data!, options: []) as? [String:AnyObject]{
                self.dict = responseJSON
            }
        });
        task.resume()
    }
    
    // syncro for TESTING
    func makeRequestTestSync() {
        
        // make API REST request
        let urlPath: String = self.config.valueForKey("api_url") as! String
        let url: NSURL = NSURL(string: urlPath)!
        let request1: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)

        // set API KEY in header
        request1.setValue(self.config.valueForKey("api_key") as? String, forHTTPHeaderField: "x-api-key")
        
        let semaphore = dispatch_semaphore_create(0)
        let task = session.dataTaskWithRequest(request1, completionHandler:{(data, response, error) in
            if error != nil{
                print(error!.localizedDescription)
                return
            }
            if let responseJSON = try! NSJSONSerialization.JSONObjectWithData(data!, options: []) as? [String:AnyObject]{
                self.dict = responseJSON
            }
            dispatch_semaphore_signal(semaphore)
        });

        task.resume()
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
    }
    
    // method to extract childrens from a single node to a dictionary for TESTING
    func getChildrensTest(dict: NSDictionary, nodeName: String) -> NSDictionary {
        
        var new_dict : [String:AnyObject] = [:]
        
        var res : Int = 0
        for i in 0..<dict["navigationEntries"]!.count {
            
            let dict2 = dict["navigationEntries"]![i] as! NSDictionary
            let type  = dict2["type"] as! String
            let label = dict2["label"] as! String
            
            // stop if node found
            if (res == 0) {
                if ((type == "node") && (label == nodeName)) {
                    res = 1
                    new_dict["navigationEntries"] = dict2["children"]
                }
            }
            
            // stop if node found
            if (res == 0) {
                for j in 0..<dict2["children"]!.count {
                    
                    let dict3 = dict2["children"]![j] as! NSDictionary
                    let type  = dict3["type"] as! String
                    let label = dict3["label"] as! String
                    
                    if ((type == "node") && (label == nodeName)) {
                        res = 1
                        new_dict["navigationEntries"] = dict3["children"]
                    }
                }
            }
        }
        
        return new_dict
    }
    
    // method to refresh webview url
    func refreshWeb() {
        
        // Add second button to main viewcontroller
        let menu = UIBarButtonItem(image: UIImage(named: "menu"), style: .Plain, target: self, action: #selector(ViewController.goMenu))
        let back = UIBarButtonItem(image: UIImage(named: "Icon-Back"), style: .Plain, target: self, action: #selector(ViewController.goBack))
        navigationItem.leftItemsSupplementBackButton = true
        navigationItem.setLeftBarButtonItems([back, menu], animated: true)

        // change url and load request in webview
        let url = NSURL (string: NSUserDefaults.standardUserDefaults().objectForKey("url") as! String)
        let requestObj = NSURLRequest(URL: url!)
        myWebView.loadRequest(requestObj)
    }
    
    // load modal viewcontroller menu and send dictionary
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let id = segue.identifier
        
        if(id == "goMenu"){
            
            let dictResult = sender as! Dictionary<String, AnyObject>
            let nav = segue.destinationViewController as! UINavigationController
            let vc = nav.topViewController as! ModalViewController
            
            vc.setDictionary(dictResult)
            vc.setTitleVC("")
            vc.setBackVC("Zurück")
        }
        
    }
    
    // MARK: Button actions methods
    func goMenu() {
        
        // if fails doesnt show nothing in menu
        if (self.dict != nil) {
            performSegueWithIdentifier("goMenu", sender: self.dict)
        }
    }
    
    func goBack() {

        // update user defaults
        NSUserDefaults.standardUserDefaults().setInteger(0, forKey: "backMain")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        // change url and load request in webview
        let url = NSURL (string: self.config.valueForKey("main_url") as! String)
        let requestObj = NSURLRequest(URL: url!)
        myWebView.loadRequest(requestObj)
        
        // Additional bar button items, set just initial menu button
        let menu = UIBarButtonItem(image: UIImage(named: "menu"), style: .Plain, target: self, action: #selector(ViewController.goMenu))
        let menu2 = UIBarButtonItem(image: UIImage(named: ""), style: .Plain, target: nil, action: nil)
        navigationItem.leftItemsSupplementBackButton = true
        navigationItem.setLeftBarButtonItems([menu,menu2] , animated: true)
    }
    
    // MARK: WebView delegate methods
    func webViewDidStartLoad(webView : UIWebView) {
        // add indicator
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    func webViewDidFinishLoad(webView : UIWebView) {
        // remove indicator
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
}

