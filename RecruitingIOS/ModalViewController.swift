//
//  ModalViewController.swift
//  RecruitingIOS
//
//  Created by TxL PRO on 16/9/16.
//  Copyright © 2016 mytoys. All rights reserved.
//

import UIKit

class ModalViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var myTable: UITableView!
    
    var items : [String] = []
    var types : [String] = []
    var links : [String] = []
    
    var dict : NSDictionary!
    var vcTitle : String!
    
    // MARK: Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // add close button to navigation bar
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "close"), style: .Plain, target: self, action: #selector(ModalViewController.goClose))
        
        // blur style in tableview
        let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Light))
        visualEffectView.frame = myTable.bounds
        myTable.backgroundView = visualEffectView
        
        // style of tableview
        myTable.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        myTable.backgroundColor = UIColor(white: 1, alpha: 0.25)
        myTable.opaque = false
        myTable.tableFooterView = UIView()
        
        // style of viewcontroller transparent
        view.backgroundColor = UIColor.clearColor()
        view.opaque = false
        
        // show logo on header
        if (vcTitle == "") {
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            imageView.contentMode = .ScaleAspectFit
            let image = UIImage(named: "logo")
            imageView.image = image
            self.navigationItem.titleView = imageView
        }
        else {
            self.navigationItem.titleView = nil
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.-
    }

    // method to extract nodes from dictionary to arrays, just first level
    func extractNodes(dict: NSDictionary) {
        
        for j in 0..<dict["children"]!.count {
            
            let dict2 = dict["children"]![j] as! NSDictionary
            let type  = dict2["type"] as! String
            let label = dict2["label"] as! String
            
            items.append(label)
            types.append(type)

            if ((type == "link") || (type == "external-link")) {
                links.append(dict2["url"] as! String)
            }
            else {
                links.append("")
            }
        }
    }
    
    // method to extract childrens from a single node to a dictionary
    func getChildrens(dict: NSDictionary, nodeName: String) -> NSDictionary {
        
        var new_dict : [String:AnyObject] = [:]
        
        var res : Int = 0
        for i in 0..<dict["navigationEntries"]!.count {
            
            let dict2 = dict["navigationEntries"]![i] as! NSDictionary
            let type  = dict2["type"] as! String
            let label = dict2["label"] as! String
            
            // stop if node found
            if (res == 0) {
                if ((type == "node") && (label == nodeName)) {
                    res = 1
                    new_dict["navigationEntries"] = dict2["children"]
                }
            }
            
            // stop if node found
            if (res == 0) {
                for j in 0..<dict2["children"]!.count {
                
                    let dict3 = dict2["children"]![j] as! NSDictionary
                    let type  = dict3["type"] as! String
                    let label = dict3["label"] as! String
                
                    if ((type == "node") && (label == nodeName)) {
                        res = 1
                        new_dict["navigationEntries"] = dict3["children"]
                    }
                }
            }
        }
        
       return new_dict
    }
    
    // MARK: Button viewcontroller setter methods title, backbutton title and dictionary
    func setTitleVC(title: String) {
        
        self.title = title
        self.vcTitle = title
    }
    
    func setBackVC(title: String) {
        
        if (title == "Zurück") {
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: title, style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        }
    }
    
    func setDictionary(dict: NSDictionary) {
        
        self.dict = dict
        
        // extract Sections
        for i in 0..<dict["navigationEntries"]!.count {
            
            let dict2 = dict["navigationEntries"]![i] as! NSDictionary
            let type  = dict2["type"] as! String
            let label = dict2["label"] as! String
            
            items.append(label)
            types.append(type)
            
            if ((type == "link") || (type == "external-link")) {
                links.append(dict2["url"] as! String)
            }
            else {
                links.append("")
            }
            
            // one more level
            if (type == "section") {
                
                extractNodes(dict2)
            }
        }
    }
    
    // MARK: Button actions methods
    func goClose() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: TableVeiw delegate methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell:CustomTableViewCell = tableView.dequeueReusableCellWithIdentifier("CustomTableViewCell")! as! CustomTableViewCell
        
        cell.myImageView?.hidden = true
        if (self.types[indexPath.row] == "node") {
            cell.myImageView?.hidden = false
            cell.myImageView?.image = UIImage(named: "arrow")
        }
        else if (self.types[indexPath.row] == "external-link") {
            cell.myImageView?.hidden = false
            cell.myImageView?.image = UIImage(named: "external")
        }
        
        cell.myLabel?.text = self.items[indexPath.row]
        
        if (self.types[indexPath.row] == "section") {
            cell.myLabel?.font = UIFont.boldSystemFontOfSize(17.0)
            cell.backgroundColor = UIColor(white: 1, alpha: 0.25)
            cell.selectionStyle = UITableViewCellSelectionStyle.None
        }
        else {
            cell.myLabel?.font = UIFont.systemFontOfSize(17.0)
            cell.backgroundColor = UIColor(white: 1, alpha: 0.45)
        }
        
        cell.myLabel?.backgroundColor = UIColor.clearColor()
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        myTable.deselectRowAtIndexPath(indexPath, animated: true)
        
        if (self.types[indexPath.row] == "node") {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("ModalViewController") as! ModalViewController
            
            vc.setDictionary(getChildrens(self.dict, nodeName: self.items[indexPath.row]))
            vc.setTitleVC(self.items[indexPath.row])
            vc.setBackVC(self.vcTitle)
            
            navigationController!.pushViewController(vc, animated: true)
        }
        else if (self.types[indexPath.row] == "link") {

            NSUserDefaults.standardUserDefaults().setObject(self.links[indexPath.row], forKey: "url")
            NSUserDefaults.standardUserDefaults().setInteger(1, forKey: "backMain")
            NSUserDefaults.standardUserDefaults().synchronize()
            
            NSNotificationCenter.defaultCenter().postNotificationName("RefreshWeb", object: nil)
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        else if (self.types[indexPath.row] == "external-link") {
            
            UIApplication.sharedApplication().openURL(NSURL(string: self.links[indexPath.row])!)
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
}
