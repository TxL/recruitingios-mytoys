//
//  CustomTableViewCell.swift
//  RecruitingIOS
//
//  Created by TxL PRO on 16/9/16.
//  Copyright © 2016 mytoys. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var myLabel: UILabel!
    @IBOutlet weak var myImageView: UIImageView!
}
