//
//  RecruitingIOSTests.swift
//  RecruitingIOSTests
//
//  Created by TxL PRO on 16/9/16.
//  Copyright © 2016 mytoys. All rights reserved.
//

import XCTest
@testable import RecruitingIOS

class RecruitingIOSTests: XCTestCase {
    
    var main_vc: ViewController!
    var modal_vc: ModalViewController!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let nav = storyboard.instantiateInitialViewController() as! UINavigationController
        
        main_vc = nav.topViewController as! ViewController
        UIApplication.sharedApplication().keyWindow!.rootViewController = main_vc
        
        XCTAssertNotNil(nav.view)
        XCTAssertNotNil(main_vc.view)
        
        testLoadConfig()
        testMakeRequestSync()
        testGetChildrens()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testLoadConfig() {
        
        let path = NSBundle.mainBundle().pathForResource("Config", ofType: "plist")
        main_vc.config = NSDictionary(contentsOfFile: path!)
        XCTAssert(main_vc.config != nil, "config file load correctlly")
    }
    
    func testMakeRequestSync() {
        
        main_vc.makeRequestTestSync()
        XCTAssert(main_vc.dict != nil, "JSON data load correctlly")
    }
    
    func testRefreshWeb() {
        
       let url_example = "http://www.mytoys.de/0-6-months/"
        
       NSUserDefaults.standardUserDefaults().setObject(url_example, forKey: "url")
       NSUserDefaults.standardUserDefaults().setInteger(1, forKey: "backMain")
       NSUserDefaults.standardUserDefaults().synchronize()
        
       NSNotificationCenter.defaultCenter().postNotificationName("RefreshWeb", object: nil)
        
       //print(main_vc.myWebView!.stringByEvaluatingJavaScriptFromString("window.location"))
    }
    
    func testGoMenu() {
        
        main_vc.goMenu()
    }
    
    func testGetChildrens() {
        
        let new_dict : [String:AnyObject] = main_vc.getChildrensTest(main_vc.dict,nodeName: "Alter") as! [String : AnyObject]
        XCTAssert(new_dict.count != 0, "childrens from node extracted correctlly")
    }
    
}
