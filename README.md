Recruiting IOS MyToys app - Jose Luis Gonzalez Moreno

  - Developed in Swift 2.X with XCode 7.3.1.
  - UI designed with storyboards and Autolayout, just tested on iPhone devices.
  - Two viewcontrollers Main and Modal, Modal has a tableview that refresh itself with new data while navigate.
  - Asynchronous REST requests, JSON parse to dictionaries with native library.
  - Nothing hardcoded, important literals like url and api keys loaded from plist files.
  - Test implemented for all app functionality, all cases successfully passed.